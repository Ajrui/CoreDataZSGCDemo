//
//  USER_COLLECTION.h
//  
//
//  Created by shaorui on 15/10/19.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface USER_COLLECTION : NSManagedObject

@property (nonatomic, retain) NSString * imageHead;
@property (nonatomic, retain) NSNumber * isCollection;
@property (nonatomic, retain) NSString * jobCompany;
@property (nonatomic, retain) NSString * jobContent;
@property (nonatomic, retain) NSString * jobTitle;

@end
