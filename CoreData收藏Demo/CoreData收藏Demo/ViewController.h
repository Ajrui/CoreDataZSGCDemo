//
//  ViewController.h
//  CoreData收藏Demo
//
//  Created by shaorui on 15/10/19.
//  Copyright (c) 2015年 CoreData收藏Demo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
- (IBAction)deleteAction:(id)sender;
//查询数据
- (IBAction)btnSelectAction:(id)sender;
//
- (IBAction)btnInsertAction:(id)sender;

@end

