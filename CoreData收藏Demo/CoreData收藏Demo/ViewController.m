//
//  ViewController.m
//  CoreData收藏Demo
//
//  Created by shaorui on 15/10/19.
//  Copyright (c) 2015年 CoreData收藏Demo. All rights reserved.
//

#import "ViewController.h"
#import "USER_INFO.h"
#import "CoreDateManager.h"

@interface ViewController ()
{
    NSMutableArray *  arrCoreData;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrCoreData  = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnSelectAction:(id)sender {


    NSMutableArray * arrUser = [[NSMutableArray alloc] init];
    
    AppDelegate *deleaget = ((AppDelegate*)[[UIApplication sharedApplication] delegate]);
        //    找到对应的实体类 table
//  = deleaget.managedObjectContext;

    
    arrUser  = [[CoreDateManager shareCoreDateManager] selectData:0 andOffset:0];
    
    
    
    for (USER_INFO * info in arrUser) {
    

        NSLog(@"%@",info.userID);

    }


}

- (IBAction)btnInsertAction:(id)sender {

    
    
    AppDelegate *deleaget = ((AppDelegate*)[[UIApplication sharedApplication] delegate]);
//    找到对应的实体类 table
    NSManagedObjectContext *context = deleaget.managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"USER_INFO" inManagedObjectContext:context];
    

    
    for (int i=0 ; i<40; i++) {
    
        USER_INFO * user = [[USER_INFO alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
        if (i==10) {
            NSNumber * ssss =[NSNumber numberWithUnsignedLongLong:100+i];
            user.userID =ssss;
            user.userName = [NSString stringWithFormat:@"wwww-%d",i];
            user.userAge = [NSString stringWithFormat:@"张四wwww-%d",i];
            user.userSex = [NSString stringWithFormat:@"账务-%d",i];
            [arrCoreData addObject:user];
        }
        else
            {
            
                NSNumber * ssss =[NSNumber numberWithUnsignedLongLong:100+i];
                user.userID =ssss;
                user.userName = [NSString stringWithFormat:@"wwww-"];
                user.userAge = [NSString stringWithFormat:@"张四wwww-"];
                user.userSex = [NSString stringWithFormat:@"账务-"];
                [arrCoreData addObject:user];
            }
        
      
    }
    
    BOOL saveYesOrNo =  [[CoreDateManager shareCoreDateManager] saveContextBool];
    if (saveYesOrNo) {
        NSLog(@"已保存 %d",saveYesOrNo);
    }
    else
        {
        NSLog(@"未保存 %d",saveYesOrNo);
        }
    
    
    
}
- (IBAction)deleteAction:(id)sender {
    
    USER_INFO * userInfo = [arrCoreData objectAtIndex:11];
    

    
    
    
    BOOL saveYesOrNo =  [[CoreDateManager shareCoreDateManager] deleteUserInfoCoreData:userInfo];

    if (saveYesOrNo) {
        NSLog(@"已删除 %d",saveYesOrNo);
    }
    else
        {
        NSLog(@"未删除 %d",saveYesOrNo);
        }
    
    
    
}
@end
