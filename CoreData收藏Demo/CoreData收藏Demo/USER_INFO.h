//
//  USER_INFO.h
//  
//
//  Created by shaorui on 15/10/19.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface USER_INFO : NSManagedObject

@property (nonatomic, retain) NSNumber * userID;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * userSex;
@property (nonatomic, retain) NSString * userAge;

@end
