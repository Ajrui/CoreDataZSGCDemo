//
//  CoreDateManager.h
//  
//
//  Created by shaorui on 15/10/19.
//
//


#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "USER_INFO.h"
#import "AppDelegate.h"
#define TableName @"USER_INFO"

@interface CoreDateManager : NSObject
+(CoreDateManager *)shareCoreDateManager;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
/**查询是否保存成功**/
- (BOOL)saveContextBool;
- (BOOL)ClearUserInfo;
- (NSURL *)applicationDocumentsDirectory;

    //插入数据 多条数据
- (void)insertCoreData:(NSMutableArray*)dataArray;
//单条数据查询
- (void)insertUserInfoCoreData:(USER_INFO*)info;
/***删除指定的***/
- (BOOL)deleteUserInfoCoreData:(USER_INFO*)deleteinfo;
    /**查询**/
- (NSMutableArray*)selectData:(int)pageSize andOffset:(int)currentPage;
    /****删除所有数据****/
- (void)deleteData;
    //更新
- (void)updateData:(NSString*)newsId withIsLook:(NSString*)islook;

@end